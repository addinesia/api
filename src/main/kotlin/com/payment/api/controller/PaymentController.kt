package com.payment.api.controller

import com.payment.api.model.Salary
import com.payment.api.model.TotalTax
import com.payment.api.service.PaymentService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/hitunggaji")
@RestController
class PaymentController(val service: PaymentService) {

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    fun countSalary(@RequestBody salary: Salary) :TotalTax = service.countSalary(salary)
}