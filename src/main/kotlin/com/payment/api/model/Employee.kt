package com.payment.api.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@AllArgsConstructor
@NoArgsConstructor
data class Employee(
    var name: String,
    var status: String,
    var child: Int,
    var country: Country
){
    enum class Country {INDONESIA, VIETNAM}
}
