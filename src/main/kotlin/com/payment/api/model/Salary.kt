package com.payment.api.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@AllArgsConstructor
@NoArgsConstructor
data class Salary(
    var employee: Employee,
    var component: List<Component>
){
//    fun getEmployee(): Employee = this.employee
//    fun getComponent(): List<Component> = this.component
}
