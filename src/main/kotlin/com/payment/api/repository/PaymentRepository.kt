package com.payment.api.repository

import com.payment.api.model.Salary
import com.payment.api.model.TotalTax
import factory.NetTax
import org.springframework.stereotype.Repository
import java.text.DecimalFormat
import java.text.NumberFormat


@Repository
class PaymentRepository {
    private val list: List<Salary> = ArrayList()


    fun countSalary( s: Salary): TotalTax {

        val salary = Salary(s.employee, s.component)
        val netTax = NetTax()

        salary.employee = s.employee
        salary.component = s.component
        list.plus(salary)
        val status: String = s.employee.status
        var child: Int = s.employee.child
        var country: String = s.employee.country.toString()
        var totalsalary = s.component.sumOf { it.amount }
        val idTax = netTax.getCountry(country)

        val t2: Double = idTax!!.salaryTax(status, child, totalsalary)
        val nf = DecimalFormat("#,###,###.00")
        val id = nf.format(t2)

        val total = TotalTax(id)
        return total

    }


}