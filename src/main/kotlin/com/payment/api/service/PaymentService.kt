package com.payment.api.service

import com.payment.api.model.Salary
import com.payment.api.model.TotalTax
import com.payment.api.repository.PaymentRepository
import org.springframework.stereotype.Service

@Service
class PaymentService(val repository: PaymentRepository) {

    fun countSalary(salary: Salary) :TotalTax = repository.countSalary(salary)
}