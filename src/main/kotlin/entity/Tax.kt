package entity

interface Tax {

    fun salaryTax(status: String, child: Int, totalsalary:Int): Double
}