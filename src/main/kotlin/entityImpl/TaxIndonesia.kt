package entityImpl

import entity.Tax

class TaxIndonesia : Tax {
    override fun salaryTax(status: String, child: Int, totalsalary: Int): Double {
        val idtax = (50000000 * 5).toDouble() / 100
        var alltax = 0.0
        var ptkp = 0
        val netto: Int
        val ysum: Int
        val mnetto: Int

        if (status === "married" && child >= 1) {
            ptkp += 75000000
        } else if (status === "married" && child == 0) {
            ptkp += 50000000

        } else if (status === "not married") {
            ptkp += 25000000
        }
        ysum = totalsalary * 12
        netto = ysum - ptkp
        if (netto <= 50000000) {
            alltax = (netto * 5).toDouble() / 100
        } else if (netto > 50000000 && netto <= 250000000) {
            alltax = (netto * 10).toDouble() / 100
        } else if (netto > 250000000) {
            mnetto = netto - 50000000
            alltax = (mnetto * 15).toDouble() / 100
        }
        return (idtax + alltax) / 12


    }

}