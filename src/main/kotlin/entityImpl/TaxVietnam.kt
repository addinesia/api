package entityImpl

import entity.Tax

class TaxVietnam : Tax {
    override fun salaryTax(status: String, child: Int, totalsalary: Int): Double {
        val vndtax: Double = (50000000 * 2.5) / 100
        val assurance: Int = 1000000
        var alltax: Double = 0.00
        var ptkp: Int = 0
        val netto: Int
        val ysum: Int
        val mnetto: Int

        if (status === "married") {
            ptkp += 30000000
        } else if (status === "not married") {
            ptkp += 15000000
        }
        ysum = totalsalary * 12
        netto = ysum - ptkp - (assurance * 12)
        if (netto <= 50000000) {
            alltax = (netto * 2.5) / 100
        } else if (netto > 50000000) {
            mnetto = netto - 50000000
            alltax = (mnetto * 7.5) / 100
        }
        return (vndtax + alltax) / 12

    }

}