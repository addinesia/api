package factory

import entity.Tax
import entityImpl.TaxIndonesia
import entityImpl.TaxVietnam


class NetTax {
    fun getCountry(country: String?): Tax? {
        if (country == null) {
            return null
        }
        if (country.equals("INDONESIA", ignoreCase = true)) {
            return TaxIndonesia()
        } else if (country.equals("VIETNAM", ignoreCase = true)) {
            return TaxVietnam()
        }
        return null
    }

}